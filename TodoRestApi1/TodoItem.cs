﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TodoRestApi1
{
    public class TodoItem
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public bool isDone { get; set; }
        public string itemName { get; set; }
    }
}
