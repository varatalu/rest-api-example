﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TodoRestApi1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        public readonly TodoContext ctx;
        public TodoController(TodoContext tdc)
        {
            ctx = tdc;
        }

        // GET: api/Todo
        [HttpGet]
        public IEnumerable<TodoItem> Get()
        {
            return ctx.TodoItems;
        }

        // POST: api/Todo
        [HttpPost]
        public void Post([FromBody] TodoItem itemToAdd)
        {
            ctx.TodoItems.Add(itemToAdd);
            ctx.SaveChanges();
        }

        [HttpPatch("{id}")]
        public void Patch(int id)
        {
            var foundItem = ctx.TodoItems.Find(id);
            if (foundItem != null)
            {
                foundItem.isDone = !foundItem.isDone; // toggle
            }
            ctx.SaveChanges();
        }

        // PUT: api/Todo/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
          
        }

     

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
