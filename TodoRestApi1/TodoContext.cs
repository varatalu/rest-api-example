﻿using Microsoft.EntityFrameworkCore;

namespace TodoRestApi1
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions options) : base(options){}
       
        public DbSet<TodoItem> TodoItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TodoItem>().ToTable("TodoItems").HasKey(x => x.id);

            modelBuilder.UseIdentityColumns();
        }
    }
}
